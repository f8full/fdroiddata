Uwaga: F-Droid wymaga uprawnień roota, aby zainstalować uprzywilejowany dodatek jako prywatną aplikację systemową.

[[org.fdroid.fdroid]] wykorzystuje uprawnienia systemowe do instalacji, aktualizacji i usuwania aplikacji. Jedynym sposobem na zdobycie tych uprawnień jest stanie się aplikacją systemową.

Tu z pomocą przychodzi uprzywilejowany dodatek - jako mała samodzielna aplikacja może być zainstalowana jako aplikacja systemowa i komunikować się z główną aplikacją poprzez AIDL IPC.

Ma to kilka zalet:

* mniejsze użycie partycji systemowej * uaktualnienia systemu nie usuwają F-Droida * proces instalacji w systemie poprzez roota jest bezpieczniejszy

Zamiast tej aplikacji dla większości użytkowników bardziej odpowiednia może być aktualizacja OTA jako plik ZIP, która nazywa się [[org.fdroid.fdroid.privileged.ota]]. Ta aplikacja służy do aktualizacji uprzywilejowanego dodatku po zainstalowaniu paczki ZIP poprzez OTA.
